<?php

require 'database.php';

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

if (isset($request)) {

	if (isset($request->sensor_id) AND isset($request->timestamp) AND isset($request->chip_id) AND isset($request->fraction)) {

		$person_id = get_one("SELECT id FROM person WHERE chip_id = {$request->chip_id}");
		$time = date('Y-m-d H:i:s', $request->timestamp / 1000);

		$success = insert(
			'timetable',
			array(
				'person_id' => $person_id,
				'sensor_id' => $request->sensor_id,
				'time'      => $time,
				'fraction'  => $request->fraction,
			)
		);

		if (! $success) {
			$errors[] = 'db_insert_failed';
		}
	} else {
		$errors[] = 'sensor_data_incomplete';
	}
} else {
	$errors[] = 'method_post_required';
}

if (count($errors)) {
	echo json_encode(array('success' => FALSE, 'errors' => $errors, 'post' => $_POST));
} else {
	echo json_encode(array('success' => TRUE));
}