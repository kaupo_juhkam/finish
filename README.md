# About

Finish is a project which demonstrates, how data from real time sensors can be saved and shown as a timetable.

# Installation:

 - import SQL dump from docs/database/sql_init.sql
 - create config.php and fill it with code found below

<?php

 define('DATABASE_HOSTNAME', '127.0.0.1');
 define('DATABASE_USERNAME', '');
 define('DATABASE_PASSWORD', '');
 define('DATABASE_DATABASE', '');

 ** To start dummy, simply open PROJECT_ROOT/dummy/ from your browser **