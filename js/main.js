var app = angular.module('finish', ['windowEventBroadcasts', 'ui']);


app.controller('timeline', ['$http', '$scope', function ($http, $scope) {
	$scope.browserState = {};

	// Check if browser is focused
	$scope.$on('$windowFocus', function () {
		$scope.browserState.idle = false;
		$scope.browserState.disconnectedNotice = true;
	});
	$scope.$on('$windowBlur', function () {
		$scope.browserState.idle = true;
	});

	$scope.items = [];
	var timer = setInterval(function () {
		// Don't update data, if browser is idle
		if (!$scope.browserState.idle) {
			$http.get('ajax-load.php').success(function (data) {
				$scope.items = data;
				$scope.browserState.disconnectedNotice = false;
			});
		}
	}, 500);

}]);
