<?php

require 'database.php';

echo json_encode(
	get_all(
		"SELECT t.sensor_id, t.person_id, t.time, t.fraction, p.start_number, p.name_f, p.name_l
		FROM timetable t
		JOIN person p
			ON t.person_id = p.id
		JOIN sensor s
			ON s.id = t.sensor_id
		ORDER BY t.time DESC, t.fraction DESC"
	)
);