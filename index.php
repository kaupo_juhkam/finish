<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" ng-app="finish"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" ng-app="finish"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" ng-app="finish"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" ng-app="finish"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Finish</title>
	<meta name="description" content="Demo app for mooncascade">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" href="css/main.css">
	<script src="js/vendor/modernizr-2.6.2.min.js"></script>

	<!-- Bootstrap -->
	<link href="css/bootstrap.min.css" rel="stylesheet">

	<!-- Font Awesome -->
	<link href="css/font-awesome.min.css" rel="stylesheet">
</head>
<body>
<!--[if lt IE 7]>
<p class="browsehappy">Te kasutate <strong>iganenud</strong> brauserit. Palun <a href="http://browsehappy.com/">uuendage brauser</a> parandamaks kasutuskogemust.</p>
<![endif]-->

<div ng-controller="timeline" ng-model="timeline" ng-show="browserState.idle" class="alert alert-danger container notice">
	<h4>Ühendus serveriga katkestati, kuna rakendus pole esiplaanil</h4></div>

<div ng-controller="timeline" ng-model="timeline" ng-show="browserState.disconnectedNotice" class="alert alert-info container notice">
	<h4>Serveriga ühendamine...</h4></div>

<div class="container" ng-model="timeline" ng-controller="timeline as tl">
	<div class="row">
		<div ng-hide="items.length" class="alert alert-info notice"><h3>Ootan anduritelt andmeid...</h3></div>
		<div class="timeline-centered" ng-show="items.length">
			<article class="timeline-entry begin">

				<div class="timeline-entry-inner" style="height: 60px; margin-top: 15px">

					<div class="timeline-icon">
						<i class="fa fa-plus"></i>
					</div>

				</div>

			</article>

			<article ng-repeat="item in items | orderBy: 'sensor_id':true | unique:'person_id'" class="timeline-entry" id="{{'person_' + item.person_id}}">

				<div class="timeline-entry-inner">

					<div class="timeline-icon" ng-class="{ 'bg-success': item.sensor_id == 2, 'bg-warning': item.sensor_id == 1 }">
						<i class="fa fa-clock-o"></i>
					</div>

					<div class="timeline-label">
						<span class="label label-default">Võistleja #{{item.start_number}}</span>
						<h2>
							<strong>{{item.name_f}} {{item.name_l}}</strong>
							<span>
								{{item.sensor_id == 2 ? 'ületas finišijoone' : 'sisenes finišikoridori'}}
								<strong ng-show="item.sensor_id == 2">{{item.time | date}}.{{item.fraction}}</strong>
							</span>
						</h2>
					</div>
				</div>

			</article>

			<article class="timeline-entry begin">

				<div class="timeline-entry-inner">

					<div class="timeline-icon">
						<i class="fa fa-minus"></i>
					</div>

				</div>

			</article>


		</div>


	</div>
</div>


<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
<script src="js/bootstrap.min.js"></script>
<script src="js/plugins.js"></script>
<script src="js/vendor/angular.min.js"></script>
<script src="js/vendor/window_event_broadcasts.min.js"></script>
<script src="js/vendor/angular-ui.min.js"></script>
<script src="js/main.js"></script>

</body>
</html>
