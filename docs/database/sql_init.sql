-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Loomise aeg: Aug 25, 2014 kell 10:24 PM
-- Serveri versioon: 5.5.35-log
-- PHP versioon: 5.3.29

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Andmebaas: `r36478_finish`
--

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `person`
--

DROP TABLE IF EXISTS `person`;
CREATE TABLE IF NOT EXISTS `person` (
`id` int(10) unsigned NOT NULL,
  `name_f` varchar(255) NOT NULL,
  `name_l` varchar(255) NOT NULL,
  `start_number` int(10) unsigned NOT NULL,
  `chip_id` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=101 ;

--
-- Andmete tõmmistamine tabelile `person`
--

INSERT INTO `person` (`id`, `name_f`, `name_l`, `start_number`, `chip_id`) VALUES
(1, 'Jeanette', 'Randolph', 1000, '100'),
(2, 'Xantha', 'Compton', 999, '101'),
(3, 'Edward', 'Bishop', 998, '102'),
(4, 'Leonard', 'Cameron', 997, '103'),
(5, 'Graiden', 'Bernard', 996, '104'),
(6, 'Yvette', 'Cole', 995, '105'),
(7, 'Riley', 'Morton', 994, '106'),
(8, 'Simon', 'Hampton', 993, '107'),
(9, 'Aladdin', 'Mcfadden', 992, '108'),
(10, 'Deirdre', 'Gross', 991, '109'),
(11, 'Macey', 'York', 990, '110'),
(12, 'Zoe', 'Wilkinson', 989, '111'),
(13, 'Chandler', 'Rodriquez', 988, '112'),
(14, 'Glenna', 'Sloan', 987, '113'),
(15, 'Jesse', 'England', 986, '114'),
(16, 'Guy', 'Meyers', 985, '115'),
(17, 'Camden', 'Brewer', 984, '116'),
(18, 'Rylee', 'Parsons', 983, '117'),
(19, 'Preston', 'Dodson', 982, '118'),
(20, 'Idola', 'Spears', 981, '119'),
(21, 'Stone', 'Morse', 980, '120'),
(22, 'Yeo', 'Potts', 979, '121'),
(23, 'Nerea', 'Gay', 978, '122'),
(24, 'Rebecca', 'Snow', 977, '123'),
(25, 'Hadassah', 'Rodgers', 976, '124'),
(26, 'Jade', 'Benton', 975, '125'),
(27, 'Jemima', 'Rollins', 974, '126'),
(28, 'Dakota', 'Wall', 973, '127'),
(29, 'Keefe', 'Shepherd', 972, '128'),
(30, 'Jermaine', 'Ball', 971, '129'),
(31, 'Amela', 'Hooper', 970, '130'),
(32, 'Buffy', 'Ward', 969, '131'),
(33, 'Zephr', 'Wolfe', 968, '132'),
(34, 'Hilary', 'Maynard', 967, '133'),
(35, 'Tanya', 'Bush', 966, '134'),
(36, 'Linus', 'Hines', 965, '135'),
(37, 'Lucius', 'Craig', 964, '136'),
(38, 'Jael', 'Bright', 963, '137'),
(39, 'Ebony', 'Austin', 962, '138'),
(40, 'Meghan', 'David', 961, '139'),
(41, 'David', 'Lambert', 960, '140'),
(42, 'Penelope', 'Britt', 959, '141'),
(43, 'Hedy', 'Benton', 958, '142'),
(44, 'Heather', 'Spencer', 957, '143'),
(45, 'Luke', 'Armstrong', 956, '144'),
(46, 'Octavia', 'Good', 955, '145'),
(47, 'Jolene', 'Cooper', 954, '146'),
(48, 'Wade', 'Curtis', 953, '147'),
(49, 'Ima', 'Berg', 952, '148'),
(50, 'Jenette', 'Higgins', 951, '149'),
(51, 'Aiko', 'Gilliam', 950, '150'),
(52, 'Ina', 'Hensley', 949, '151'),
(53, 'Mechelle', 'Barber', 948, '152'),
(54, 'Lesley', 'Robertson', 947, '153'),
(55, 'Hoyt', 'Dillon', 946, '154'),
(56, 'Erin', 'Mcknight', 945, '155'),
(57, 'Lavinia', 'Mckay', 944, '156'),
(58, 'Jayme', 'Garza', 943, '157'),
(59, 'Jade', 'Santana', 942, '158'),
(60, 'Helen', 'Lee', 941, '159'),
(61, 'Myles', 'Hensley', 940, '160'),
(62, 'Kirk', 'Santiago', 939, '161'),
(63, 'Bevis', 'Howe', 938, '162'),
(64, 'Karleigh', 'Waller', 937, '163'),
(65, 'Lacy', 'Hewitt', 936, '164'),
(66, 'Warren', 'Horn', 935, '165'),
(67, 'Chadwick', 'Mitchell', 934, '166'),
(68, 'Cailin', 'Gentry', 933, '167'),
(69, 'Trevor', 'Pollard', 932, '168'),
(70, 'Karen', 'Ryan', 931, '169'),
(71, 'Kalia', 'Dominguez', 930, '170'),
(72, 'Noah', 'Espinoza', 929, '171'),
(73, 'Aiko', 'Hendricks', 928, '172'),
(74, 'Malik', 'Ware', 927, '173'),
(75, 'Alec', 'Valencia', 926, '174'),
(76, 'Christopher', 'Dickerson', 925, '175'),
(77, 'Sybil', 'Wood', 924, '176'),
(78, 'Chase', 'Walter', 923, '177'),
(79, 'Brenda', 'Campbell', 922, '178'),
(80, 'Colette', 'Elliott', 921, '179'),
(81, 'Louis', 'Pugh', 920, '180'),
(82, 'Bert', 'Cox', 919, '181'),
(83, 'Hayfa', 'Hawkins', 918, '182'),
(84, 'Kaitlin', 'Solomon', 917, '183'),
(85, 'Patrick', 'Jenkins', 916, '184'),
(86, 'Devin', 'Meadows', 915, '185'),
(87, 'Yardley', 'Olsen', 914, '186'),
(88, 'Scarlett', 'White', 913, '187'),
(89, 'Darius', 'Rose', 912, '188'),
(90, 'Selma', 'Colon', 911, '189'),
(91, 'Ignacia', 'Downs', 910, '190'),
(92, 'Britanney', 'Jones', 909, '191'),
(93, 'Calista', 'Blanchard', 908, '192'),
(94, 'Regina', 'Gilbert', 907, '193'),
(95, 'Ira', 'Mcmahon', 906, '194'),
(96, 'Ursa', 'Mccullough', 905, '195'),
(97, 'Rose', 'Barker', 904, '196'),
(98, 'Ishmael', 'Richmond', 903, '197'),
(99, 'Heidi', 'Huffman', 902, '198'),
(100, 'Karleigh', 'Houston', 901, '199');

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `sensor`
--

DROP TABLE IF EXISTS `sensor`;
CREATE TABLE IF NOT EXISTS `sensor` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Andmete tõmmistamine tabelile `sensor`
--

INSERT INTO `sensor` (`id`, `name`) VALUES
(1, 'finish-gate'),
(2, 'finish-line');

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `timetable`
--

DROP TABLE IF EXISTS `timetable`;
CREATE TABLE IF NOT EXISTS `timetable` (
  `person_id` int(10) unsigned NOT NULL,
  `sensor_id` int(10) unsigned NOT NULL,
  `time` timestamp NULL DEFAULT NULL,
  `fraction` int(11) DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indeksid tõmmistatud tabelitele
--

--
-- Indeksid tabelile `person`
--
ALTER TABLE `person`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indeksid tabelile `sensor`
--
ALTER TABLE `sensor`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indeksid tabelile `timetable`
--
ALTER TABLE `timetable`
 ADD UNIQUE KEY `person_id` (`person_id`,`sensor_id`), ADD KEY `fk_timetable_person_idx` (`person_id`), ADD KEY `fk_timetable_sensor1_idx` (`sensor_id`);

--
-- AUTO_INCREMENT tõmmistatud tabelitele
--

--
-- AUTO_INCREMENT tabelile `person`
--
ALTER TABLE `person`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT tabelile `sensor`
--
ALTER TABLE `sensor`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Tõmmistatud tabelite piirangud
--

--
-- Piirangud tabelile `timetable`
--
ALTER TABLE `timetable`
ADD CONSTRAINT `fk_timetable_person` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_timetable_sensor1` FOREIGN KEY (`sensor_id`) REFERENCES `sensor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
