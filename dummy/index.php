<html ng-app="dummy">
<head>
	<title>DummyDataSender</title>
	<!-- Bootstrap -->
	<link href="/css/bootstrap.min.css" rel="stylesheet">

	<!-- Font Awesome -->
	<link href="/css/font-awesome.min.css" rel="stylesheet">

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

</head>
<body ng-controller="dummyController as dummy">
<div class="well container">
	<h4>Koosta ise järjekord, milles andmed saadetakse</h4>


	<form ng-submit="addNew()" ng-model="dummy">
		<div style="width: 20%; margin-right: 10px" class="pull-left">
			<label for="sensor_id">Andur</label>
			<select id="sensor_id" ng-model="new_entry.sensor_id" class="form-control" required="">
				<option value="1">1 - koridor</option>
				<option value="2">2 - joon</option>
			</select>
		</div>

		<div style="width: 20%; margin-right: 10px" class="pull-left">
			<label for="chip_id">Kiibi ID</label>
			<input ng-model="new_entry.chip_id" id="chip_id" type="number" min="0" class="form-control" required="">
		</div>

		<div style="width: 20%; margin-right: 10px" class="pull-left">
			<label for="time">Millal lisada? (millisekundid)</label>
			<input ng-model="new_entry.time" id="time" type="number" min="0" step="1" class="form-control" required="">
		</div>

		<button type="submit" class="btn btn-primary">Lisa</button>
	</form>

</div>

<div class="container well" ng-show="timetable.length">
	<a ng-click="run()" class="btn btn-success">Käivita DEMO</a>
	<br/>
	<br/>

	<table class="table table-responsive">
		<tr>
			<th>Andur</th>
			<th>Kiibi ID</th>
			<th>Aeg</th>
		</tr>
		<tr ng-model="dummy" ng-repeat="item in timetable">
			<td>{{item.sensor_id}}</td>
			<td>{{item.chip_id}}</td>
			<td>{{item.time}}</td>
		</tr>
	</table>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/vendor/angular.min.js"></script>
<script type="text/javascript">

	// For sorting array by specified value
	function dynamicSort(property) {
		var sortOrder = 1;
		if (property[0] === "-") {
			sortOrder = -1;
			property = property.substr(1);
		}
		return function (a, b) {
			var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
			return result * sortOrder;
		}
	}

	// Starting some angular magic
	var app = angular.module('dummy', []);

	app.controller('dummyController', ['$http', '$scope', function ($http, $scope) {

		$scope.timetable = [];
		$scope.new_entry = {sensor_id: 1};

		$scope.addNew = function () {
			$scope.timetable.push($scope.new_entry);
			$scope.new_entry = {sensor_id: $scope.new_entry.sensor_id};

			$scope.timetable.sort(dynamicSort("time"));
			console.log($scope.timetable);
		};

		$scope.run = function () {

			var time = 0;
			var timer = setInterval(function () {
				if (time == $scope.timetable[0].time) {
					var post_data = {
						sensor_id: $scope.timetable[0].sensor_id,
						chip_id  : $scope.timetable[0].chip_id,
						timestamp: (new Date()).getTime(),
						fraction : (new Date()).getMilliseconds()
					};

					$http.post('/ajax-save.php', post_data);

					$scope.timetable.shift();
				}

				if (!$scope.timetable.length) {
					clearInterval(timer);
				}

				time++;
			}, 1);

		};

	}]);
</script>

</body>
</html>